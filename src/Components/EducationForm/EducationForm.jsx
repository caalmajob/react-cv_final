import { useDispatch } from 'react-redux';
import { addEducationExperience } from '../../redux/slices/curriculum.slice';
import { useState } from 'react';
import './EducationForm.scss';


const INITIAL_STATE = {
    title: "",
    company: "",
    description: "",
}

const EducationForm = (props) => {
const [education, setEducation] = useState(INITIAL_STATE);
const dispatch = useDispatch();

   const handleFormSubmit = (ev) => {
        ev.preventDefault();
        console.log( 'Formulario Recibido', education)
        dispatch(addEducationExperience(education));
        setEducation(INITIAL_STATE);
    }

  const handleInputChange = (ev) => {
        const {name, value} = ev.target;
        setEducation({ ...education, [name]: value });
    }
    
        return (
            <div className="form">
                <form onSubmit={handleFormSubmit}>
                    <label>
                        <p className="form__info">Title:</p>
                        <input type="text"
                            name="title" value={education.title}
                            onChange={handleInputChange} />
                    </label>

                    <label>
                        <p className="form__info">Company:</p>
                        <input type="text"
                            name="company"
                            value={education.company}
                            onChange={handleInputChange} />
                    </label>

                    <label>
                        <p className="form__info">Description:</p>
                        <textarea className="textarea"
                            type="text" name="description"
                            value={education.description}
                            onChange={handleInputChange} />
                    </label>

                    <div className="form__container">
                        <button className="form__button" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
        )
}

export default EducationForm;