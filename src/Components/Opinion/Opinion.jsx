import { useState } from 'react';
import { useSelector } from 'react-redux';
import storage from "../../services/storage";
import { FormOpinion } from '../../Components';
import './Opinion.scss';

const INITIAL_OPINIONS = storage.getItem('opinion') || [];

const Opinion = (props) => {
    const [opinion, setOpinion] = useState(INITIAL_OPINIONS);
    const { user } = useSelector(state => state.user);
    const [openForm, setOpenForm] = useState(false);

    const handleOpenForm = () => {
        setOpenForm(!openForm);
    }


    const addNewOpinion = (newopinion) => {
        setOpinion([...opinion, newopinion]);
        storage.setItem('opinion', [...opinion, newopinion]);
    }

    return (
        <div className="opinion">
            <div className="opinion__center">

                <h2 className="opinion__title">Opinions</h2>
                <h4>Have you worked or studied with me?: Register and add an opinion about my work.</h4>
                <ul>
                    {opinion.map((el, index) => {
                        return (
                            <li className="opinion__list"
                                key={`${el}-${index}`}>
                                <p className="opinion__number"><strong>Autor/a:</strong> {el.username}</p>
                                <p className="opinion__desc">{el.info}</p>
                            </li>
                        );
                    })}
                </ul>

                {user && <div className="opinion__form">
                    <button className="jobs__button" onClick={handleOpenForm}>
                        Add a new opinion</button>
                    {openForm && (
                        <FormOpinion addNewOpinion={addNewOpinion} />
                    )}
                </div>}
            </div>
        </div>
    )
}

export default Opinion;


