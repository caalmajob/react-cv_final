import { useSelector } from 'react-redux';
import './Skills.scss';


const Skills = (props) => {
    const { skill, language, others } = useSelector(state => state.curriculum);

    return (
        <div className="skill">
            <div className="skill__center">
                <h2 className="skill__title">Skills</h2>
                <div className="skill__row">
                    <div>
                        <h3>_Technologies:</h3>
                        <ul>
                            {skill.map((el, index) => {
                                return (
                                    <li className="jobs__list"
                                        key={`${el}-${index}`}>
                                        <p className="job__title">- {el}</p>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div><h3>_Competencies:</h3>
                        {others.map((el, index) => {
                            return (
                                <li className="jobs__list"
                                    key={`${el}-${index}`}>
                                    <p className="job__title">- {el}</p>
                                </li>
                            );
                        })}</div>
                    <div><h3>_Languages:</h3>
                        {language.map((el, index) => {
                            return (
                                <li className="jobs__list"
                                    key={`${el}-${index}`}>
                                    <p className="job__title">- {el}</p>
                                </li>
                            );
                        })}</div>


                </div>
            </div>
        </div>
    )
}

export default Skills;