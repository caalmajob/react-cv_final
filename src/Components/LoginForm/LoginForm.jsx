import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginAsync } from '../../redux/slices/user.slice';
import { Link } from "react-router-dom";
import './LoginForm.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
}

const LoginForm = (props) => {
    const [formData, setFormData] = useState(INITIAL_STATE);
    const { error } = useSelector(state => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async ev => {
        ev.preventDefault();
        await dispatch(loginAsync(formData));
        setFormData(INITIAL_STATE);
    };

    const handleInputChange = ev => {
        const { name, value } = ev.target;

        setFormData({ ...formData, [name]: value });
    }

    return (
        <div className="login-form">
            <div className="login-form__center">

                <h3>LOGIN</h3>

                <form onSubmit={handleFormSubmit}>
                    <label htmlFor="email">
                        <p>Email</p>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder="Email"
                            onChange={handleInputChange}
                            value={formData.email}
                        />
                    </label>

                    <label htmlFor="password">
                        <p>Password</p>
                        <input
                            type="password"
                            name="password"
                            id="password"
                            placeholder="Contraseña"
                            onChange={handleInputChange}
                            value={formData.password}
                        />
                    </label>

                    <div className="login-form__button">
                        <button type="submit">Acceder</button>
                    </div>
                </form>
                {error && <div className="login-form__error">
                    {error}
                </div>}
                <button className="login-form__return"><Link to={{ pathname: "/" }}>Return</Link></button>
            </div>
        </div>

    )
}

export default LoginForm;
