import RegisterForm from './RegisterForm/RegisterForm';
import LoginForm from './LoginForm/LoginForm';
import Navbar from './ Navbar/Navbar';
import SecureRoute from './SecureRoute/SecureRoute';
import Form from './Form/Form';
import Header from './Header/Header';
import Jobs from './Jobs/Jobs';
import EducationItem from './EducationItem/EducationItem';
import Education from './Education/Education';
import Opinion from './Opinion/Opinion';
import FormOpinion from './FormOpinion/FormOpinion';
import EducationForm  from './EducationForm/EducationForm';
import Info  from './Info/Info';
import Skills  from './Skills/Skills';
import NavbarMain from './NavbarMain/NavbarMain';







export {
    RegisterForm,
    LoginForm,
    Navbar,
    SecureRoute,
    Form,
    Header,
    Jobs,
    EducationItem,
    Education,
    Opinion,
    FormOpinion,
    EducationForm,
    Info,
    Skills,
    NavbarMain,
}
