import { useSelector } from 'react-redux';
import './Header.scss';

const Header = () => {
    const { fullName, title, info, image } = useSelector(state => state.curriculum);
   
    return (
        <header className="header">
            <div className="header__left">
                <div>
                    <img className="header__img"
                        src={image} alt="perfil"/>
                </div>
            </div>
            <div className="header__right">
                <div><p className="header__right-title">{title}</p></div>
                <div><p className="header__right-name">{fullName}</p></div>
                <div><p className="header__right-info">{info}</p></div>
            </div>
        </header>
    )
}

export default Header;