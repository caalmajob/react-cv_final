import { useDispatch, useSelector } from 'react-redux';
import { logoutAsync } from '../../redux/slices/user.slice';
import { Link } from "react-router-dom";
import './Navbar.scss';

const Navbar = () => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.user);

    return (
        <nav className="nav">
            {!user && <div className="nav__button">
                <button><Link to={{ pathname: "/login" }}>LOGIN</Link></button>
                <button><Link to={{ pathname: "/register" }}>REGISTER</Link></button>
            </div>}

            {user && <div className="nav">
                <div className="nav__user">
                        <h4>Welcome!: {user.username}</h4>
                        <button onClick={() => dispatch(logoutAsync())}>Logout</button>
                </div>
            </div>}
        </nav>
    )
}

export default Navbar;
