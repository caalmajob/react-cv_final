import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Form } from '../../Components';
import './Jobs.scss';

const Jobs = () => {
    const { experience } = useSelector(state => state.curriculum);
    const { user } = useSelector(state => state.user);
    const [openForm, setOpenForm] = useState(false);

    const handleOpenForm = () => {
        setOpenForm(!openForm);
    }

    if (user === undefined || user === null) {
        return (<div className="jobs">
        <div className="jobs__center">
            <h2 className="jobs__title">Experience</h2>
            <ul>
                {experience.map((el, index) => {
                    return (
                        <li className="jobs__list"
                            key={`${el}-${index}`}>
                            <h3 className="job__title">_{el.title}</h3>
                            <p className="job__company">{el.company}</p>
                            <p className="job__desc">{el.description}</p>
                        </li>
                    );
                })}
            </ul>
        </div>
    </div>);
    }

    if (user.role === 'admin') {
        return (
            <div className="jobs">
                <div className="jobs__center">
                    <h2 className="jobs__title">Experience</h2>
                    <ul>
                        {experience.map((el, index) => {
                            return (
                                <li className="jobs__list"
                                    key={`${el}-${index}`}>
                                    <h3 className="job__title">_{el.title}</h3>
                                    <p className="job__company">{el.company}</p>
                                    <p className="job__desc">{el.description}</p>
                                </li>
                            );
                        })}
                    </ul>

                    {user && <div className="jobs__form">
                        <button className="jobs__button" onClick={handleOpenForm}>
                            Add a Job Experience</button>
                        {openForm && (
                            <Form />
                        )}
                    </div>}
                </div>
            </div>
        )
    }

    if (user.role === 'basic') {
        return (<div className="jobs">
            <div className="jobs__center">
                <h2 className="jobs__title">Experience</h2>
                <ul>
                    {experience.map((el, index) => {
                        return (
                            <li className="jobs__list"
                                key={`${el}-${index}`}>
                                <h3 className="job__title">_{el.title}</h3>
                                <p className="job__company">{el.company}</p>
                                <p className="job__desc">{el.description}</p>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>);
    }
}

export default Jobs;