import { useState } from 'react';
import { useSelector } from 'react-redux';
import { EducationItem } from '../../Components';
import { EducationForm } from '../../Components';
import './Education.scss';


const Education = () => {
    const { education } = useSelector(state => state.curriculum);
    const [openForm, setOpenForm] = useState(false);
    const { user } = useSelector(state => state.user);

    const handleOpenForm = () => {
        setOpenForm(!openForm);
    }



    if (user === undefined || user === null) {
        return (<div className="education">
            <div className="education__center">
                <h2 className="education__title">Education</h2>
                <ul>
                    {education.map((el, index) => {
                        return (
                            <li className="education__list" key={`${el}-${index}`}>
                                <EducationItem education={el} />
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>);
    }

    if (user.role === 'admin') {
        return (<div className="education">
            <div className="education__center">
                <h2 className="education__title">Education</h2>
                <ul>
                    {education.map((el, index) => {
                        return (
                            <li className="education__list" key={`${el}-${index}`}>
                                <EducationItem education={el} />
                            </li>
                        );
                    })}
                </ul>


                <div className="opinion__form">
                    <button className="opinion__button" onClick={handleOpenForm}>
                        Add a Education Experience</button>
                </div>

                {openForm && (
                    <EducationForm />
                )}
            </div>
        </div>
        )
    }

    if (user.role === 'basic') {
        return(<div className="education">
            <div className="education__center">
                <h2 className="education__title">Education</h2>
                <ul>
                    {education.map((el, index) => {
                        return (
                            <li className="education__list" key={`${el}-${index}`}>
                                <EducationItem education={el} />
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>);
    }
}
export default Education;
