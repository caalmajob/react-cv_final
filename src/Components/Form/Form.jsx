import { useDispatch } from 'react-redux';
import { addJobExperience } from '../../redux/slices/curriculum.slice';
import { useState } from 'react';
import './Form.scss';


const INITIAL_STATE = {
    title: "",
    company: "",
    description: "",
}

const Form = (props) => {
const [job, setJob] = useState(INITIAL_STATE);
const dispatch = useDispatch();

   const handleFormSubmit = (ev) => {
        ev.preventDefault();
        console.log( 'Formulario Recibido', job)
        dispatch(addJobExperience(job));
        setJob(INITIAL_STATE);
    }

  const handleInputChange = (ev) => {
        const {name, value} = ev.target;
        setJob({ ...job, [name]: value });
    }
    
        return (
            <div className="form">
                <form onSubmit={handleFormSubmit}>
                    <label>
                        <p className="form__info">Title:</p>
                        <input type="text"
                            name="title" value={job.title}
                            onChange={handleInputChange} />
                    </label>

                    <label>
                        <p className="form__info">Company:</p>
                        <input type="text"
                            name="company"
                            value={job.company}
                            onChange={handleInputChange} />
                    </label>

                    <label>
                        <p className="form__info">Description:</p>
                        <textarea className="textarea"
                            type="text" name="description"
                            value={job.description}
                            onChange={handleInputChange} />
                    </label>

                    <div className="form__container">
                        <button className="form__button" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
        )
}

export default Form;