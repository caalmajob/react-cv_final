import { useSelector } from 'react-redux';
import { useState } from 'react';
import './FormOpinion.scss';


const INITIAL_OPINIONS = {
    info: "",
    username: "",
}

const FormOpinion = (props) => {
    const [opinion, setOpinion] = useState(INITIAL_OPINIONS);
    const { user } = useSelector(state => state.user);

    const handleFormSubmit = (ev) => {
        ev.preventDefault();
        props.addNewOpinion(opinion);
        setOpinion(INITIAL_OPINIONS);
    }


    const handleInputChange = (ev) => {
        const { name, value } = ev.target;
        setOpinion({ ...opinion, [name]: value });
    }

    return (
        <div className="form">
            {user && <div>
                <form onSubmit={handleFormSubmit}>
                    <label htmlFor="username">
                        <input
                            type="hidden"
                            name="username"
                            id="text"
                            onChange={handleInputChange}
                            value={opinion.username = user.username}
                        />
                    </label>
                    <label>
                        <p className="form__info">Insert Your Opinion</p>
                        <textarea className="textarea"
                            type="text" name="info"
                            value={opinion.info}
                            onChange={handleInputChange} />
                    </label>

                    <div className="form__container">
                        <button className="form__button" type="submit">Enviar</button>
                    </div>
                </form>
            </div>}

        </div>
    )
}

export default FormOpinion;