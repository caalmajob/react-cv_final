import { useSelector } from 'react-redux';
import './Info.scss';


const Info = (props) => {
    const { city, phone, email } = useSelector(state => state.curriculum);

    return (
        <div className="info">
            <div className="info__center">
                <h2 className="info__title">Contact & Info</h2>
                <p className="info__paragraph"><strong>_City: </strong>{city}</p>
                <p className="info__paragraph"><strong>_Phone: </strong> {phone}</p>
                <p className="info__paragraph"><strong>_Email: </strong> {email}</p>
            </div>
        </div>

    )
}

export default Info;