import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

const SecureRoute = (props) => {
    const {hasUser} = useSelector(state => state.user);

    if(hasUser === null) {
        return (<div>Cargando ...</div>);
    }

    if(hasUser) {
        return (<Route {...props} />);
    }

    if(!hasUser) {
        return (<Redirect to="/login" />);
    }
};

export default SecureRoute;
