import './EducationItem.scss';

const EducationItem = (props) => {

        return (
            <div className="educationItem">
                <h3 className="educationItem__title">_{props.education.title}</h3>
                <p className="educationItem__center">{props.education.company}</p>
                <p className="educationItem__desc">{props.education.description}</p>
            </div>
        )
}

export default EducationItem;