import { Link } from "react-router-dom";
import { faAddressBook, faBookOpen, faBriefcase, faClipboardList, faFeatherAlt, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './NavbarMain.scss';

const NavbarMain = () => {

    return (
            <div className="main__navbar">
            <Link to={{pathname: "/"}}><FontAwesomeIcon icon={faHome} /></Link>
            <Link to={{pathname: "/education"}}><FontAwesomeIcon icon={faBookOpen} /></Link>
            <Link to={{pathname: "/jobs"}}><FontAwesomeIcon icon={faBriefcase} /></Link>
            <Link to={{pathname: "/Skills"}}><FontAwesomeIcon icon={faClipboardList} /></Link>
            <Link to={{pathname: "/info"}}><FontAwesomeIcon icon={faAddressBook} /></Link>
            <Link to={{pathname: "/Opinion"}}><FontAwesomeIcon icon={faFeatherAlt} /></Link>
            </div>
    )
}

export default NavbarMain;
