import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { registerAsync } from '../../redux/slices/user.slice';
import { Link } from "react-router-dom";

import './RegisterForm.scss';

const INITIAL_STATE = {
    username: '',
    email: '',
    password: '',
};

const RegisterForm = (props) => {
    const [formFields, setFormFields] = useState(INITIAL_STATE);
    const { error } = useSelector(state => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async (ev) => {
        ev.preventDefault();
        dispatch(registerAsync(formFields))
        setFormFields(INITIAL_STATE);
    };

    const handleInputChange = (ev) => {
        const { name, value } = ev.target;

        setFormFields({ ...formFields, [name]: value });

        console.log(formFields);
    };

    return (
        <div className="register-form">
            <div className="register-form__center">
                <h3>REGISTER</h3>
                <form onSubmit={handleFormSubmit}>
                    <label htmlFor="username">
                        <p>Username</p>
                        <input
                            type="text"
                            name="username"
                            id="username"
                            placeholder="Username"
                            onChange={handleInputChange}
                            value={formFields.username}
                        />
                    </label>

                    <label htmlFor="email">
                        <p>Email</p>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder="Email"
                            onChange={handleInputChange}
                            value={formFields.email}
                        />
                    </label>

                    <label htmlFor="password">
                        <p>Password</p>
                        <input
                            type="text"
                            name="password"
                            id="password"
                            placeholder="Password"
                            onChange={handleInputChange}
                            value={formFields.password}
                        />
                    </label>

                    <div className="register-form__button">
                        <button type="submit">Register</button>
                    </div>

                </form>
                {error && <div className="register-form__error">
                    {error}
                </div>}
                <button className="register-form__return"><Link to={{ pathname: "/" }}>Return</Link></button>

            </div>
        </div>
    )
}

export default RegisterForm;
