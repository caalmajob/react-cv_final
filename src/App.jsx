import { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { checkSessionAsync } from './redux/slices/user.slice';
import {
  RegisterForm,
  LoginForm,
  Navbar,
  Header,
  Jobs,
  Education,
  Opinion,
  Info,
  Skills,
  NavbarMain,
  // SecureRoute
} from './Components';
import './App.scss';


const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    getUser();
  });


  const getUser = async () => {
    dispatch(checkSessionAsync());
  }

  return (
    <Router>
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/register" component={(props) => <RegisterForm {...props} />} />
          <Route exact path="/login" component={(props) => <LoginForm {...props} />} />
          <div className="App__container">
            <div>
              <Header />
              <NavbarMain />

            </div>
            <Switch>
              <div className="App__main">
                <div>
                  <Route exact path="/education" component={(props) => <Education{...props} />} />
                </div>
                <div>
                  <Route exact path="/jobs" component={(props) => <Jobs {...props} />} />
                </div>
                <div>
                  <Route exact path="/opinion" component={(props) => <Opinion {...props} />} />
                </div>
                <div>
                  <Route exact path="/skills" component={(props) => <Skills {...props} />} />
                </div>
                <div>
                  <Route exact path="/info" component={(props) => <Info {...props} />} />
                </div>
              </div>
            </Switch>
          </div>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
