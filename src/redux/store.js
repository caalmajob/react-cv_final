import { configureStore } from '@reduxjs/toolkit';
import { userSlice } from './slices/user.slice';
import { curriculumSlice } from './slices/curriculum.slice';

export default configureStore({
    reducer: {
        user: userSlice.reducer,
        curriculum: curriculumSlice.reducer,
    }
});