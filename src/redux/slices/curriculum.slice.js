import { createSlice } from '@reduxjs/toolkit';

const INITIAL_STATE = {
    fullName: 'Carlos Alcalá Martos',
    title: 'Junior Full Stack Developer',
    info: "Hello!, I'm a historian who has decided to make a radical change in his life. That's why I'm training as a Full Stack Developer at Upgrade Hub. I am active and passionate about improving and learning new things.",
    image: 'https://media-exp1.licdn.com/dms/image/C4D03AQHoeDqQvdmwWQ/profile-displayphoto-shrink_800_800/0/1610802007806?e=1623888000&v=beta&t=EsRKUQQe6wTHpkGaGyTUT5kMYmVxp83bj2vMEmnlPPs',
    city: 'Parla (Comunidad de Madrid)',
    phone: '(+34) 658107896',
    email: 'caalmajob@gmail.com',
    age: '29',
    skill: [ 'HTML5 & CSS3', 'SCSS', 'JavaScript', 'PHP', 'Symfony', 'SQL & MongoDB', 'Node.js', 'React', 'Angular', 'Agile'],
    language: ['Native Spanish', 'English: B1 Cambridge'],
    others: ['Teamwork', 'Ability to work under pressure', 'Organised and consistent', 'Proactive and assertive'],
    education: [
        { title: 'Degree in History', company: 'Granada University', description: 'Organized in four years, it focuses on the history of Europe, the Near East, and the Americas.' },
        { title: "Master's  in Compulsory Secondary Education, Baccalaureate, Vocational Training and Language Teaching", company: 'Granada University', description: 'Training in all fields of teaching for teaching in high schools. With 200 hours of practical training.' },
        { title: "Full Stack Developer", company: 'Upgrade Hub', description: 'Web development, applications and digital products from scratch. Focused on both back-end and front-end. HTML, CSS, JavaScript, PHP, Angular, Symfony, React, NodeJS, Git among others.' },
    ],
    experience: [
        { title: 'Cashier/Warehouse Operative', company: 'Carrefour (Madrid, ES)', description: 'Tasks mainly focused on customer service at the cash desk and organisation of the warehouse.' },
        { title: 'Senior Team Member', company: 'Ahi Poke (London, UK)', description: 'Intermediate position with tasks focused on customer service, problem solving at staff or store level (lack of stock, system failures..., etc.), orientation and coordination of employees, as well as accounting, banking, management and processing of orders.' },
        { title: 'TL', company: 'ITSU (London, UK)', description: 'I work in a Japanese food restaurant, with tasks focused on sushi preparation, customer service as well as staff organization and training...etc.' },
        { title: 'Teacher', company: 'Colegio Regina Mundis', description: 'Teaching History and Geography subjects in Secondary and Baccalaureate.' },
    ],
}

export const curriculumSlice = createSlice({
    name: 'curriculum',
    initialState: INITIAL_STATE,
    reducers: {
        addJobExperience: (state, action) => {
            state.experience = [...state.experience, action.payload ];
        },
        addEducationExperience: (state, action) => {
            state.education = [...state.education, action.payload ];
        },
    },
});


export const { addJobExperience, addEducationExperience } = curriculumSlice.actions;
