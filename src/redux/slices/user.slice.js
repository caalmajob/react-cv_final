import { createSlice, createAsyncThunk, current } from "@reduxjs/toolkit";
import { checkSession, login, logout, register } from "../../api/auth";

export const checkSessionAsync = createAsyncThunk(
  "user/checkSession",
  async () => {
    return await checkSession();
  }
);

export const loginAsync = createAsyncThunk("user/login", async (form) => {
  return await login(form);
});

export const registerAsync = createAsyncThunk("user/register", async (form) => {
  return await register(form);
});

export const logoutAsync = createAsyncThunk("user/logout", async () => {
  return await logout();
});

export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: null,
    hasUser: null,
    error: "",
  },
  reducers: {
    logoutUser: async (state, action) => {
        const actualState = current(state);
        console.log('global state', actualState);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(logoutAsync.fulfilled, (state, action) => {
      state.user = null;
      state.hasUser = null;
      state.error = "";
    })
    builder.addCase(checkSessionAsync.fulfilled, (state, action) => {
      if (!action.payload.message) {
        state.user = action.payload;
        state.hasUser = true;
        state.error = "";
      } else {
        state.hasUser = false;
      }
    });
    builder.addCase(loginAsync.fulfilled, (state, action) => {
      if (!action.payload.message) {
        state.user = action.payload;
        state.hasUser = true;
        state.error = "";
      } else {
        state.hasUser = false;
        state.error = action.payload.message;
      }
    });
    builder.addCase(registerAsync.fulfilled, (state, action) => {
      if (!action.payload.message) {
        state.user = action.payload;
        state.hasUser = true;
        state.error = "";
      } else {
        state.hasUser = false;
        state.error = action.payload.message;
      }
    });
  },
});

export const { logoutUser } = userSlice.actions;
